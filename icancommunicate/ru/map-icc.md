ПРИ ПОДГОТОВКЕ К УРОКУ

Это упражнение с готовым содержанием, которое нельзя дополнить. 
Можно изменить только размер людей, настроив его с помощью функции «Change size» на панели управления size2.

НА УРОКЕ

Для работы с упражнением ставьте галочку в нужной ячейке и отрабатывайте грамматические конструкции, перемещая людей в нужные страны. 
Например, 
Who are they? Where do they live? What language do they speak? Where do you live? Do russians speak english?
Who speaks english? Are they happy? Why are they crying? Are they at home? .. etc.
